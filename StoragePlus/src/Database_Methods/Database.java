/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database_Methods;

import Logic.Book;
import Logic.Errors;
import Logic.Library;
import com.db4o.Db4o;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.*;
import java.util.ArrayList;
import java.util.ListIterator;

/**
 *
 * @author Fery
 */
public class Database {

    private static String DB4OFILENAME = "books.db4o";
    //Aqui esta el objeto y la configuracion por defecto de la base de datos
    private static ObjectContainer db = Db4o.openFile(Db4o.newConfiguration(), DB4OFILENAME);
    //Metodo para añadir objetos a la base de datos

    public static void anadirObjeto(Book e) {
        getDb().store(e);
        System.out.println("Libro añadido correctamente");
    }

    //Metodo para buscar libros
//    public static Book devolverObjeto(Book e) {
//        Book eh = new Book();
//        ObjectSet resul = db.queryByExample(eh);
//        return listResult(resul, e);
//    }
//
//    //Metodo para buscar el objeto (query)
    public static Book buscarLibro(ListIterator e, Book es) {
        Book b = null;
        while (e.hasNext()) {
           b = (Book) e.next();
            if (b.getName().equalsIgnoreCase(es.getName())) {
               return b;
           }
       }
       return null;
   }
    public static ArrayList todalaBase() {
        ArrayList<Book> libros = new ArrayList();
        Book eh = new Book();
        ObjectSet resul = db.queryByExample(eh);
        while (resul.hasNext()) {
            libros.add((Book) resul.next());
        }
        return libros;
    }
    
    public static void updateBook(Book e,String nombre, String numberPage, String actualPage) throws Errors,NumberFormatException{
        ObjectSet set =db.queryByExample(e);
        Book found =(Book) set.next();
        found.setName(nombre);
        found.setActualPage(actualPage);
        found.setNumberOfPages(Integer.parseInt(numberPage));
        Library.testBook(found);
        db.store(found);
        
    }

    //Metodo para buscar y eliminar objetos
    public static boolean deleteObject(Book e) {
////        ObjectSet result = db.queryByExample(db);
////        Pilot found = (Pilot) result.next();
        ArrayList base = todalaBase();
        int num = 0;
        boolean con = false;
        while (base.listIterator().hasNext()) {
            Book bo =(Book) base.get(num);
            if (e.getName().equalsIgnoreCase(bo.getName())) {
                db.delete(bo);
                con = true;
                return con;
            }
            num++;
        }
        if (con = false) {
            System.out.println("No se ha borrado correctamente");
        }else{
            System.out.println("Se ha borrado correctamente");
        }
        return con;
    }

    /**
     * @return the db
     */
    public static ObjectContainer getDb() {
        return db;
    }
}
