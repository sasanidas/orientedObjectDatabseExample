/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaz_GUI;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Fery
 */
public class Notification_Window {
    
    public static void MensajeOK(JFrame e,String mensaje){
        JOptionPane.showMessageDialog(e, mensaje, "Ok", JOptionPane.INFORMATION_MESSAGE);
    }
    public static void MensajeError(JFrame e,String mensaje){
                JOptionPane.showMessageDialog(e, mensaje, "Fail", JOptionPane.ERROR_MESSAGE);
    }
}
