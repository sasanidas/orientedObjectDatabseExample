
package Logic;

/**
 *
 * @author Fery
 */
public class Book {
    private String name;
    private int numberOfPages;
    private String actualPage;

    public Book(){}
    public Book(String name, int numberOfPages, String actualPage) {
        this.name = name;
        this.numberOfPages = numberOfPages;
        this.actualPage = actualPage;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the numberOfPages
     */
    public int getNumberOfPages() {
        return numberOfPages;
    }

    /**
     * @param numberOfPages the numberOfPages to set
     */
    public void setNumberOfPages(int numberOfPages) {
        this.numberOfPages = numberOfPages;
    }

    /**
     * @return the actualPage
     */
    public String getActualPage() {
        return actualPage;
    }

    /**
     * @param actualPage the actualPage to set
     */
    public void setActualPage(String actualPage) {
        this.actualPage = actualPage;
    }
    
    
    
}
