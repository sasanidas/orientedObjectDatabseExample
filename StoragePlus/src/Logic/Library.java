package Logic;

import java.util.ArrayList;
import java.util.ListIterator;

// This is the library, where are all the methos that we going to use
// to interac with the books.
public class Library {

    private static ArrayList<Book> library = new ArrayList();

    //We looking for a book in our database and return it.
    public static Book returnBook(Book b) throws Errors {
        Book e = new Book();
        ListIterator list = getLibrary().listIterator();
        while (list.hasNext()) {
            e = (Book) list.next();
            if (e.getName().equalsIgnoreCase(b.getName())) {
                return e;
            }
        }
        throw new Errors();
    }

    public static Book testBook(Book e) throws Errors {
        if (e.getName().length() >= 20 || e.getName().length() == 0) {
            //Length not valid
            throw new Errors("Longitud del nombre no valida");
        }
        if (Integer.parseInt(e.getActualPage()) >= e.getNumberOfPages() || Integer.parseInt(e.getActualPage()) < 0) {
            //Page not valid
            throw new Errors("Pagina no valida");
        }
        if (e.getNumberOfPages() > 1000) {
            //Book too long.
            throw new Errors("Libro demasiado largo");
        }
        return e;
    }

    /**
     * @return the library
     */
    public static ArrayList<Book> getLibrary() {
        return library;
    }

    /**
     * @param aLibrary the library to set
     */
    public static void setLibrary(ArrayList<Book> aLibrary) {
        library = aLibrary;
    }

}
